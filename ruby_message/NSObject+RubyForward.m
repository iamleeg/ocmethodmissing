//
//  NSObject+RubyForward.m
//  ruby_message
//
//  Created by Graham Lee on 30/11/2013.
//  Copyright (c) 2013 Graham Lee. All rights reserved.
//

#import "NSObject+RubyForward.h"
#import <objc/runtime.h>

id method_missing(id self, SEL _cmd, ...);

@implementation NSObject (GJLRubyForward)

+ (void)load
{
    objc_setForwardHandler(method_missing, method_missing);
}

- (id)methodMissing:(SEL)aSelector :(NSArray *)arguments
{
    if ([arguments count])
    {
        NSMutableString *string = [NSStringFromSelector(aSelector) mutableCopy];
        for (id argument in arguments)
        {
            [string appendFormat: @" %@", argument];
        }
        return [string autorelease];
    }
    else return NSStringFromSelector(aSelector);
}

@end

NSUInteger arguments_in_selector(SEL aSelector)
{
    NSString *name = NSStringFromSelector(aSelector);
    return [[name componentsSeparatedByString:@":"] count] - 1;
}

id method_missing(id self, SEL _cmd, ...)
{
    NSUInteger argumentCount = arguments_in_selector(_cmd);
    NSMutableArray *argumentList = [NSMutableArray array];
    if (argumentCount > 0) {
        va_list list;
        va_start(list, _cmd);
        for (NSUInteger i = 0; i < argumentCount; i++) {
            id thisArgument = va_arg(list, id);
            [argumentList addObject:thisArgument];
        }
        va_end(list);
    }
    return [self methodMissing:_cmd :argumentList];
}